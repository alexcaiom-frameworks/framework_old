package br.com.waiso.framework.abstratas;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.GregorianCalendar;
import java.util.List;

import br.com.waiso.framework.utils.UtilsData;
import br.com.waiso.framework.abstratas.TipoMetodo;
import br.com.waiso.framework.abstratas.utils.ReflectionUtils;

/**
 * @author Alex
 *
 */
public abstract class Classe {

	public String CLASSE_NOME = getClass().getSimpleName(); 
	
	/**
	 * Metodo Logger especializado
	 */
	protected void log(String textoParaLog) {
		try {
			System.out.println("Log "+ CLASSE_NOME +" em  " + UtilsData.calendarToStringDataCompleta(GregorianCalendar.getInstance()) + " - " + textoParaLog);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Metodo que verifica se o objeto eh nulo
	 * @param o
	 * @return
	 */
	public static boolean naoExiste(Object o){
		boolean naoExiste = o==null;
		if (o instanceof String) {
			String texto = (String) o;
			naoExiste = texto.equals("null");
		}
		return naoExiste;
	}
	
	/**
	 * Metodo que verifica se o objeto nao eh nulo
	 * @param o
	 * @return
	 */
	public static boolean existe(Object o){
		boolean existe = !naoExiste(o);
		return existe;
	}
	
	/**
	 * Informe a instancia de um objeto, o Tipo de Metodo (get/set) e o atributo do qual deseja o metodo
	 * @param o
	 * @param tipoMetodo
	 * @param atributo
	 * @return
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 */
	public static Method getMetodo (Class<?> classe, TipoMetodo tipoMetodo, String atributo) throws SecurityException {
		Method metodo = ReflectionUtils.getMetodo(classe, tipoMetodo, atributo);
		return metodo;
	}
	
	public Class<?> getTipoGenerico(List<?> lista) {
		return ReflectionUtils.getTipoGenerico(lista);
	}
}